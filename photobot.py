import telebot
import urllib.request
import filters
import cvstuff
import os

token = '1486731226:AAGfGBEZylWTR5G9IB1sYhaDIJ1wWrgH9tc'
bot = telebot.TeleBot(token)

photo_path = ""
current_photos = []
color = -1

main_menu = telebot.types.ReplyKeyboardMarkup(True)
main_menu.row('Змінити', 'Фільтри')
main_menu.row('Корекція кольору', 'Закінчити корекцію')
main_menu.row('Скасувати', 'Завершити роботу')

filters_menu = telebot.types.ReplyKeyboardMarkup(True)
filters_menu.row('Чорно-білий', "Негатив")
filters_menu.row('Ретро', 'Малиновий')
filters_menu.row('Назад', 'Скасувати')

colcor_menu = telebot.types.ReplyKeyboardMarkup(True)
colcor_menu.row("Червоний", "Помаранчевий", "Жовтий")
colcor_menu.row("Зелений", "Голубий", "Синій", "Пурпурний")
colcor_menu.row("Назад")

edits_menu = telebot.types.ReplyKeyboardMarkup(True)
edits_menu.row('Яскравість +', 'Яскравість -')
edits_menu.row('Деталі +', 'Деталі -')
edits_menu.row('Кадрувати', 'Колір +')
edits_menu.row('Назад', 'Скасувати')

messages = {
    "Чорно-білий": filters.gray_scale,
    "Негатив": filters.negative,
    "Ретро": filters.retro,
    "Малиновий": filters.raspberry,
    "Яскравість +": filters.brighten,
    "Яскравість -": filters.darken,
    "Кадрувати": filters.cut,
    "Деталі +": filters.sharpen,
    "Деталі -": filters.blur,
    "Колір +": filters.hue_plus,
}

colors = {
    "Червоний": 10,
    "Помаранчевий": 20,
    "Жовтий": 30,
    "Зелений": 60,
    "Голубий": 90,
    "Синій": 120,
    "Пурпурний": 150,
}


def apply_filter(filter_func, photo_path, message):
    photo_path = filter_func(photo_path)
    photo = open(photo_path, 'rb')
    bot.send_photo(message.chat.id, photo, caption="Ваше зображення:")
    photo.close()
    update_edits_list(photo_path)
    return photo_path


def apply_filter_to_color(filter_func, photo_path, color,  message):
    photo_path = cvstuff.apply_and_merge(photo_path, color, filter_func)
    photo = open(photo_path, 'rb')
    bot.send_photo(message.chat.id, photo, caption="Ваше зображення:")
    photo.close()
    update_edits_list(photo_path)
    return photo_path


def update_edits_list(photo_path):
    global current_photos
    current_photos.append(photo_path)


def clear_edits_list():
    global current_photos
    for i in current_photos:
        print(i)
        os.remove(i)
    current_photos.clear()


def undo():
    global current_photos
    global photo_path
    del current_photos[len(current_photos) - 1]
    photo_path = current_photos[len(current_photos) - 1]

print("bot started...")

@bot.message_handler(commands=['start'])
def send_greet_message(message):
    bot.send_message(
        message.chat.id, 'Привіт! Я бот-фоторедактор.\nЩоб почати редагувати фото, надішліть його файлом.')


@bot.message_handler(content_types=['document'])
def get_photo(message):
    document_id = message.document.file_id
    file_info = bot.get_file(document_id)
    urllib.request.urlretrieve(
        f'https://api.telegram.org/file/bot{token}/{file_info.file_path}', file_info.file_path)
    global photo_path
    photo_path = file_info.file_path
    print(photo_path)
    update_edits_list(photo_path)
    bot.send_message(
        message.chat.id, "Чудове фото! Тепер оберіть дію в меню.", reply_markup=main_menu)


@bot.message_handler(content_types=['photo'])
def wrong_file_message(message):
    bot.send_message(
        message.chat.id, "Бодь ласка, віправте фото у вигляді файлу. Це можна зробити через 'прикріпити файл'->'Обрати з галереї'.")


@bot.message_handler(content_types=['sticker', 'video', 'voice', 'audio'])
def handle_different_filetypes(message):
    bot.send_message(
        message.chat.id, 'Це не схоже на картинку. Я працюю лише з зображеннями :)')


@bot.message_handler(commands=['sendlast'])
def send_photos(message):
    bot.send_photo(message.chat.id, open(photo_path, 'rb'))


@bot.message_handler(content_types=['text'])
def menu_click_handler(message):
    global photo_path
    global color
    if message.text == 'Фільтри':
        bot.send_message(message.chat.id, "Оберіть фільтр",
                         reply_markup=filters_menu)
    elif message.text == 'Змінити':
        bot.send_message(message.chat.id, "Оберіть дію",
                         reply_markup=edits_menu)
    elif message.text == 'Корекція кольору':
        bot.send_message(message.chat.id, "Оберіть колір для подальшої обробки",
                     reply_markup=colcor_menu)
    elif message.text == 'Закінчити корекцію':
        bot.send_message(message.chat.id, "Закінчено. Тепер усі дії застосовуються до повної фотографії",
                     reply_markup=main_menu)
        color = -1
    elif message.text == 'Назад':
        bot.send_message(
            message.chat.id, "Выберите действие в меню.", reply_markup=main_menu)
    elif message.text == 'Завершити роботу':
        photo_path = filters.save_pretty(photo_path)
        photo = open(photo_path, 'rb')
        bot.send_document(message.chat.id, photo, caption="Ваша фотографія:")
        photo.close()
        clear_edits_list()
        photo_path = ""
    elif message.text == 'Скасувати':
        if (len(current_photos) > 1):
            undo()
            photo = open(photo_path, 'rb')
            bot.send_photo(message.chat.id, photo,
                           caption="Дію скасовано. Ваша фотографія:")
    if message.text in colors:
        color = colors[message.text]
        bot.send_message(message.chat.id, "Оберіть фільтр для обраного кольору",
                         reply_markup=filters_menu)
    if message.text in messages:
        if(photo_path != '' and color < 0):
            photo_path = apply_filter(
                messages[message.text], photo_path, message)
        elif(color > 0):
            photo_path = apply_filter_to_color(messages[message.text], photo_path, color, message)
        else:
            bot.send_message(message.chat.id, "Спочатку відправте фото!")
    

bot.polling(none_stop=True, interval=0)
