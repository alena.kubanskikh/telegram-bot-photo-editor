from cv2 import cv2
import numpy as np
import filters 
import os

mask = 0

#применяет фильтр к маске изображения и сливает их вместе 
def apply_and_merge(source_name, color, filter_func):
    #создание маски и наложение фильтра
    img = cv2.imread(source_name)
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    lower_color = np.array([color-15, 50, 50])
    upper_color = np.array([color+15, 255, 255])
    mask = cv2.inRange(hsv, lower_color, upper_color)
    foreground = cv2.bitwise_and(img, img, mask=mask)
    cv2.imwrite("masked.jpg", foreground)
    image_masked = cv2.imread(filter_func("masked.jpg"))
    fg = cv2.bitwise_and(image_masked, image_masked, mask=mask)
    mask = cv2.bitwise_not(mask)
    bk = cv2.bitwise_and(img, img, mask=mask)
    final = cv2.bitwise_or(fg, bk)
    cv2.imwrite(source_name+"f.jpg", final)
    os.remove("masked.jpg")
    return source_name+"f.jpg"
