from PIL import Image, ImageEnhance

#filtering
def negative(source_name):
    source = Image.open(source_name).convert('RGB')
    result = Image.new('RGB', source.size)
    for x in range(source.size[0]):
        for y in range(source.size[1]):
            r, g, b = source.getpixel((x, y))
            result.putpixel((x, y), (255 - r, 255 - g, 255 - b))
    result.save(source_name+"e.png", "JPEG")
    return source_name+"e.png"

def gray_scale(source_name):
    source = Image.open(source_name).convert('RGB')
    result = Image.new('RGB', source.size)
    for x in range(source.size[0]):
            for y in range(source.size[1]):
                r, g, b = source.getpixel((x, y))
                gray = int(r * 0.2126 + g * 0.7152 + b * 0.0722)
                result.putpixel((x, y), (gray, gray, gray))
    result_name = source_name+"e.png"
    result.save(result_name, "JPEG")
    print(result_name)
    return result_name

def retro(source_name):
    source = Image.open(source_name).convert('RGB')
    result = Image.new('RGB', source.size)
    for x in range(source.size[0]):
        for y in range(source.size[1]):
            r, g, b = source.getpixel((x, y))
            red = int(r * 1.3)
            green = int(g * 1.1)
            blue = int(b * 0.8)
            result.putpixel((x, y), (red, green, blue))
    result.save(source_name+"e.png", "JPEG")
    return source_name+"e.png"

def raspberry(source_name):
    source = Image.open(source_name).convert('RGB')
    result = Image.new('RGB', source.size)
    for x in range(source.size[0]):
        for y in range(source.size[1]):
            r, g, b = source.getpixel((x, y))
            red = int(r * 3)
            green = int(g)
            blue = int(b)
            result.putpixel((x, y), (red, green, blue))
    result.save(source_name+"e.png", "JPEG")
    return source_name+"e.png"

#editing 
def brighten(source_name):
    brightness = 1.2
    source = Image.open(source_name).convert('RGB')
    result = Image.new('RGB', source.size)
    for x in range(source.size[0]):
        for y in range(source.size[1]):
            r, g, b = source.getpixel((x, y))

            red = int(r * brightness)
            red = min(255, max(0, red))

            green = int(g * brightness)
            green = min(255, max(0, green))

            blue = int(b * brightness)
            blue = min(255, max(0, blue))

            result.putpixel((x, y), (red, green, blue))
    result.save(source_name+"e.png", "JPEG")
    return source_name+"e.png"

def darken(source_name):
    brightness = 0.8
    source = Image.open(source_name).convert('RGB')
    result = Image.new('RGB', source.size)
    for x in range(source.size[0]):
        for y in range(source.size[1]):
            r, g, b = source.getpixel((x, y))

            red = int(r * brightness)
            red = min(255, max(0, red))

            green = int(g * brightness)
            green = min(255, max(0, green))

            blue = int(b * brightness)
            blue = min(255, max(0, blue))

            result.putpixel((x, y), (red, green, blue))
    result.save(source_name+"e.png", "JPEG")
    return source_name+"e.png"

def hue_plus(source_name):
    source = Image.open(source_name).convert('RGB')
    result = Image.new('RGB', source.size)
    for x in range(source.size[0]):
        for y in range(source.size[1]):
            r, g, b = source.getpixel((x, y))
            red = int(r * 2.0)
            green = int(g)
            blue = int(b)
            result.putpixel((x, y), (red, green, blue))
    result.save(source_name+"e.png", "JPEG")
    return source_name+"e.png"

def cut(source_name):
    source = Image.open(source_name).convert('RGB')
    result = Image.new('RGB', source.size)
    result = source.crop((100,100,400,400))
    result.save(source_name+"e.png", "JPEG")
    return source_name+"e.png"

def sharpen(source_name):
    source = Image.open(source_name).convert('RGB')
    result = Image.new('RGB', source.size)
    result = ImageEnhance.Sharpness(source).enhance(2)
    result.save(source_name+"e.png", "JPEG")
    return source_name+"e.png"

def blur(source_name):
    source = Image.open(source_name).convert('RGB')
    result = Image.new('RGB', source.size)
    result = ImageEnhance.Sharpness(source).enhance(0.1)
    result.save(source_name+"e.png", "JPEG")
    return source_name+"e.png"

def save_pretty(source_name):
    source = Image.open(source_name).convert('RGB')
    source.save("yourimage.png", "PNG")
    return "yourimage.png"
